var Client = require('origami-client');

function CraneClientFactory(crane) {
  if (!crane) throw new Error('crane is required');
  
  this.crane = crane;
}

CraneClientFactory.prototype.createClient = function (token, notifications) {
  var self = this;
  
  return new Promise(function (resolve, reject) {
    var subsocket;
    
    self
    .crane
    .createSocket(
      '',
      function (socket, params) {
        subsocket = socket;
        
        return Promise.resolve();
      }
    )
    .catch(function () {
      reject();
    })
    .then(function () {
      subsocket.emit(
        'describe-apis', 
        function (err, apis) {
          if (err) return reject(err);
          
          var client = new Client(subsocket, apis, token, notifications);
          
          resolve(client);
        }
      );
    });
  });
};



module.exports = CraneClientFactory;