var CraneClientFactory = require('..');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('CraneClientFactory', function () {
  it('exports a function', function () {
    assert.equal('function', typeof(CraneClientFactory));
  });
  
  it('requires a crane', function () {
    assert.throws(
      function () {
        new CraneClientFactory();
      },
      /crane is required/
    );
  });
  
  describe(
    '.createClient', 
    function () {
      it(
        'rejects if createSocket fails', 
        function (done) {
          var factory = new CraneClientFactory({
            createSocket: function () {
              return Promise.reject();
            }
          });
        
          factory
          .createClient()
          .catch(function () {
            done();
          });
        }
      );
    }
  );
  
  describe(
    '.createClient', 
    function () {
      it(
        'emits describe-apis if createSocket resolves', 
        function (done) {
          var factory = new CraneClientFactory({
            createSocket: function (
              namespace,
              handler
            ) {
              var fakeSocket = new EventEmitter();
              fakeSocket
              .on(
                'describe-apis',
                function (callback) {
                  try {
                    assert.equal('function', typeof(callback));
                    
                    done();
                  } catch (e) {
                    done(e);
                  }
                }
              );
              
              handler(fakeSocket, {});
              
              return Promise.resolve();
            }
          });
        
          factory
          .createClient();
        }
      );
      
      it(
        'resolves client on successful describe-apis callback', 
        function (done) {
          var factory = new CraneClientFactory({
            createSocket: function (
              namespace,
              handler
            ) {
              var fakeSocket = new EventEmitter();
              fakeSocket
              .on(
                'describe-apis',
                function (callback) {
                  callback(
                    null, 
                    {
                      Plugin1: {
                        'method1': ['param1']
                      }
                    }
                  );
                }
              );
              
              handler(fakeSocket, {});
              
              return Promise.resolve();
            }
          });
        
          factory
          .createClient()
          .then(function (client) {
            try {
              assert(client);
              assert(client.Plugin1);
              assert(client.Plugin1.method1);
              
              done();
            } catch (e) {
              done(e);
            }
          });
        }
      );
      
      it(
        'rejects on describe-apis error callback', 
        function (done) {
          var factory = new CraneClientFactory({
            createSocket: function (
              namespace,
              handler
            ) {
              var fakeSocket = new EventEmitter();
              fakeSocket
              .on(
                'describe-apis',
                function (callback) {
                  callback(
                    'some error'
                  );
                }
              );
              
              handler(fakeSocket, {});
              
              return Promise.resolve();
            }
          });
        
          factory
          .createClient()
          .catch(function (err) {
            try {
              assert.equal('some error', err);
              
              done();
            } catch (e) {
              done(e);
            }
          });
        }
      );
    }
  );
});